from django import forms

from .models import Company


class CompanyForm(forms.ModelForm):

    class Meta:
        model = Company
        fields = ('title', 'brand', 'legal_name', 'description', 'logo_img', 'email', 'website_url', 'phone', 'address', 'categories', )
        widgets = {
            'categories': forms.CheckboxSelectMultiple(),
        }
