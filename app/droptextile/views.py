from django.shortcuts import render
from django.views import generic
from django.db.models import Count, Q
from django.core.paginator import Paginator
from django.urls import reverse_lazy


from .models import Category, Company
from .forms import CompanyForm


def add_company_success(request):
    return render(request, 'add_company_success.html')


def rules(request):
    return render(request, 'rules.html')


class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = "companies.html"
    slug_field = 'url'
    slug_url_kwarg = 'url'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category_list'] = Category.objects.annotate(num_companies=Count('company', filter=Q(company__is_approved=True))).order_by('order_id')
        company_list = Company.objects.filter(categories=context['category'], is_approved=True).order_by('title')
        page = self.request.GET.get('page')
        context['company_list'] = Paginator(company_list, 12).get_page(page)
        context['current_category'] = Category.objects.get(url=self.kwargs['url'])
        context['num_all'] = Company.objects.filter(is_approved=True).count()
        return context


class CompanyListView(generic.ListView):
    model = Company
    template_name = "companies.html"
    paginate_by = 12
    queryset = Company.objects.filter(is_approved=True).order_by('title')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category_list'] = Category.objects.annotate(num_companies=Count('company', filter=Q(company__is_approved=True))).order_by('order_id')
        context['num_all'] = Company.objects.filter(is_approved=True).count()
        return context


class CompanyDetailView(generic.DetailView):
    model = Company
    template_name = "company.html"
    slug_field = 'url'
    slug_url_kwarg = 'url'


class AddCompanyView(generic.CreateView):
    form_class = CompanyForm
    template_name = "add_company.html"
    success_url = reverse_lazy('add_company_success')

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.created_by = self.request.user
        obj.save()
        return super().form_valid(form)
