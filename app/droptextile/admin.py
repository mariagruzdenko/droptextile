import datetime

from django.contrib import admin
from django.db.utils import ProgrammingError
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
from adminsortable2.admin import SortableAdminMixin

from .models import Category, Company, SiteSettings


@admin.register(Category)
class AdminCategory(SortableAdminMixin, admin.ModelAdmin):
    list_display = ('title', 'url', 'order_id')
    readonly_fields = ('url', 'order_id')


@admin.register(Company)
class AdminCompany(admin.ModelAdmin, DynamicArrayMixin):
    readonly_fields = ('created_by', 'approved_by', 'approved_at', 'url')
    list_display = ('title', 'created_by', 'approved_by', 'approved_at', 'is_approved')
    filter_horizontal = ('categories',)

    def save_model(self, request, obj, form, change):
        if not change:
            obj.created_by = request.user
            if obj.is_approved:
                obj.approved_at = datetime.datetime.now()
                obj.approved_by = request.user

        else:
            if 'is_approved' in form.changed_data:
                if obj.is_approved:
                    obj.approved_at = datetime.datetime.now()
                    obj.approved_by = request.user
                else:
                    obj.approved_at = None
                    obj.approved_by = None
        super().save_model(request, obj, form, change)


class SiteSettingsAdmin(admin.ModelAdmin):

    list_display = ('title', 'site_url')
    fields = ('title', 'site_url', 'description', 'h1', 'after_body_script', 'before_body_end_script', 'header_script')

    # Создадим объект по умолчанию при первом страницы SiteSettingsAdmin со списком настроек
    def __init__(self, model, admin_site):
        super().__init__(model, admin_site)
        # обязательно оберните загрузку и сохранение SiteSettings в try catch,
        # чтобы можно было выполнить создание миграций базы данных
        try:
            SiteSettings.load().save()
        except ProgrammingError:
            pass

    # запрещаем добавление новых настроек
    def has_add_permission(self, request, obj=None):
        return False

    # а также удаление существующих
    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(SiteSettings, SiteSettingsAdmin)