from django.apps import AppConfig


class DropTextileConfig(AppConfig):
    name = 'droptextile'
