import time
import secrets
import string
import sys
from unidecode import unidecode

from django.contrib.auth import admin
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django_better_admin_arrayfield.models.fields import ArrayField
from phonenumber_field.modelfields import PhoneNumberField

from PIL import Image
from io import BytesIO


def get_upload_to(instance, filename):
    alphabet = string.ascii_lowercase + string.digits
    filename = '%s-%s' % (''.join(secrets.choice(alphabet) for _ in range(16)), filename)
    return time.strftime(filename)


def ensure_image_compress(img):
    if not img:
        return img

    im1 = Image.open(img)
    im1.load()
    img_fmt = im1.format

    w, h = im1.size
    if w > 800:
        stream = BytesIO()
        im1.resize((800, int(h/(w/800)))).save(stream, format=img_fmt)

        stream.seek(0)
        filename = img.name
        img = InMemoryUploadedFile(stream, None, filename, 'image/jpeg', sys.getsizeof(stream), None)

        # im2 = Image.new('RGBA', im1.size, (255, 255, 255))
        # im1.load()
        # im2.paste(im1, mask=im1.split()[3])
        #
        # stream = BytesIO()
        # im2.save(stream, format='JPEG2000', quality=95)
        #
        # stream.seek(0)
        # filename = '%s.jpg' % img.name.split('.')[0]
        # img = InMemoryUploadedFile(stream, None, filename, 'image/jpeg', sys.getsizeof(stream), None)

    return img


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(SingletonModel, self).save(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


class Category(models.Model):
    class Meta:
        db_table = 'droptextile_categories'
        verbose_name_plural = 'Категории'
        verbose_name = 'Категория'
        ordering = ['order_id']

    id = models.AutoField(primary_key=True)
    title = models.CharField(
        max_length=1024,
        verbose_name='Название',
        unique=True)
    url = models.SlugField(
        max_length=1024,
        null=False,
        blank=True,
        default="")
    h1 = models.CharField(
        verbose_name='Заголовок h1',
        max_length=256,
        blank=True,
        null=True)
    title_html = models.TextField(
        max_length=1024,
        blank=True,
        null=True)
    description_html = models.TextField(
        max_length=4 * 1024,
        blank=True,
        null=True)
    seo_text = models.TextField(
        max_length=100 * 1024,
        blank=True,
        null=True)
    order_id = models.PositiveIntegerField(
        default=0,
        blank=False,
        null=False)

    def __str__(self):
        return f'{self.title}'

    def save(self, *args, **kwargs):
        self.url = slugify(unidecode(self.title))
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('category', args=[str(self.url)])


class Company(models.Model):
    class Meta:
        db_table = 'droptextile_companies'
        verbose_name_plural = 'Компании'
        verbose_name = 'Компания'

    id = models.AutoField(primary_key=True)
    categories = models.ManyToManyField(
        Category,
        blank=True,
        verbose_name='Категории')
    title = models.CharField(
        max_length=1024,
        verbose_name='Название',
        unique=True)
    url = models.SlugField(
        max_length=1024,
        null=False,
        unique=True,
        default="")
    brand = models.CharField(
        max_length=1024,
        blank=True,
        null=True,
        verbose_name='Бренд')
    legal_name = models.CharField(
        max_length=1024,
        unique=True,
        verbose_name='Юридическое имя')
    description = models.TextField(
        max_length=20 * 1024,
        blank=True,
        null=True,
        verbose_name='Описание')
    logo_img = models.ImageField(
        upload_to=get_upload_to,
        blank=True,
        null=True,
        verbose_name='Лого')
    email = models.EmailField(
        blank=True,
        null=True,
        verbose_name='Эл. почта')
    phone = ArrayField(
        PhoneNumberField(),
        verbose_name='Телефоны')
    address = models.TextField(
        max_length=4 * 1024,
        blank=True,
        null=True,
        verbose_name='Адрес')
    website_url = models.URLField(
        blank=True,
        null=True,
        verbose_name='Вэб-сайт')
    title_html = models.TextField(
        max_length=1024,
        blank=True,
        null=True)
    description_html = models.TextField(
        max_length=40 * 1024,
        blank=True,
        null=True)
    is_approved = models.BooleanField(
        blank=False,
        null=False,
        default=False,
        verbose_name='Прошла проверку')
    approved_at = models.DateTimeField(
        blank=True,
        null=True,
        verbose_name='Дата проверки')
    approved_by = models.ForeignKey(
        admin.User,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name='approved_by_user',
        verbose_name='Проверена кем')
    created_by = models.ForeignKey(
        admin.User,
        on_delete=models.DO_NOTHING,
        blank=True,
        null=True,
        related_name='created_by_user',
        verbose_name='Создана кем')

    def __str__(self):
        return f'{self.title}'

    def save(self, *args, **kwargs):
        self.url = slugify(unidecode(self.title))
        self.logo_img = ensure_image_compress(self.logo_img)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('company', args=[str(self.url)])


class SiteSettings(SingletonModel):
    class Meta:
        db_table = 'droptextile_site_settings'
        verbose_name_plural = 'Site Settings'
        verbose_name = 'Site Settings'

    id = models.AutoField(primary_key=True)
    site_url = models.URLField(
        verbose_name='Website url',
        max_length=256)
    title = models.CharField(
        verbose_name='Title',
        max_length=256)
    h1 = models.CharField(
        verbose_name='Заголовок h1',
        max_length=256,
        blank=True,
        null=True)
    description = models.TextField(
        max_length=20 * 1024,
        blank=True,
        null=True,
        verbose_name='Описание')
    after_body_script = models.TextField(
        max_length=40 * 1024,
        null=True,
        blank=True,
        default="",
        verbose_name='Скрипты после тега <body>')
    before_body_end_script = models.TextField(
        max_length=40 * 1024,
        null=True,
        blank=True,
        default="",
        verbose_name='Скрипты перед тегом </body>')
    header_script = models.TextField(
        max_length=40 * 1024,
        blank=True,
        null=True,
        default="",
        verbose_name='Скрипты внутри тега </head>')

    def __str__(self):
        return f'{self.title}'
