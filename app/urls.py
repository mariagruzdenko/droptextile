"""droptextile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include
from django.views.generic.base import TemplateView
from allauth.account.decorators import login_required

from sitemaps import CompanySitemap, CategorySitemap
from droptextile import views

sitemaps = {
    'companies': CompanySitemap,
    'categories': CategorySitemap,
}


urlpatterns = (
    path('', views.CompanyListView.as_view(), name='index'),
    path('rules', views.rules, name='rules'),
    path('robots.txt', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}),
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('company/add', login_required(views.AddCompanyView.as_view()), name="add_company"),
    path('company/add_company_success', views.add_company_success, name="add_company_success"),
    path('company/<slug:url>', views.CompanyDetailView.as_view(), name="company"),
    path('company/<int:pk>', views.CompanyDetailView.as_view(), name="company_pk"),
    path('category/<slug:url>/page<int:page>/', views.CompanyDetailView.as_view, name="category_company"),
    path('category/<slug:url>', views.CategoryDetailView.as_view(), name="category"),
)
