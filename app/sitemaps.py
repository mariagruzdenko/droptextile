from django.contrib.sitemaps import Sitemap
from droptextile.models import Company, Category


class CompanySitemap(Sitemap):

    def items(self):
        return Company.objects.all()


class CategorySitemap(Sitemap):

    def items(self):
        return Category.objects.all()
