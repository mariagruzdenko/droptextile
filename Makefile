SERVER  ?= cloudtextile
PROJECT ?= droptextile
HOST    ?= localhost

PY     ?= python3
PIP    ?= pip3
DOCKER ?= docker
DC     ?= docker-compose
DC     := $(DC) --compatibility --project-name=$(PROJECT)
GIT    ?= git

TAG ?= $(shell $(GIT) tag --points-at HEAD)
TAG := $(if $(TAG),$(TAG),latest)

## usage: print this message
usage: Makefile
	@echo
	@echo 'Usage: make <TARGETS> ... <OPTIONS>'
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' | sed -e 's/^/ /'
	@echo
	@echo 'By default print this message.'
	@echo

## venv: python -m venv ...
.PHONY: venv
venv:
	@echo " ---> [$@]"
	$(PY) -m venv $(PWD)/venv

## freeze: pip freeze > requirements.txt
.PHONY: freeze
freeze:
	@echo " ---> [$@]"
	$(PIP) freeze > app/requirements.txt

## deps: pip install --requirement requirements.txt
.PHONY: deps
deps:
	@echo " ---> [$@]"
	$(PIP) install --requirement app/requirements.txt

## migrations: manage.py makemigrations
.PHONY: migrations
migrations:
	@echo " ---> [$@]"
	$(PWD)/app/manage.py makemigrations

## createsuperuser: manage.py createsuperuser
.PHONY: createsuperuser
createsuperuser:
	@echo " ---> [$@]"
	DJANGO_SUPERUSER_USERNAME=admin \
    DJANGO_SUPERUSER_PASSWORD=admin \
    	$(PWD)/app/manage.py createsuperuser --noinput --email='admin@mail.com'

## static: collect static
.PHONY: static
static:
	@echo " ---> [$@]"
	STATICFILES=nginx/static \
		$(PWD)/app/manage.py collectstatic --noinput

## image-nginx: build docker image with staticfiles
.PHONY: image-nginx
image-nginx:
	@echo " ---> [$@]"
	@-mkdir nginx/static
	$(DOCKER) build $(PWD)/nginx \
		--tag $(PROJECT)/nginx:$(TAG) \
		--tag $(PROJECT)/nginx:latest

## image-django: build docker image with django app
.PHONY: image-django
image-django:
	@echo " ---> [$@]"
	$(DOCKER) build $(PWD)/app \
		--tag $(PROJECT)/django:$(TAG) \
		--tag $(PROJECT)/django:latest

## dev-start: rebuild and restart docker-compose, dev mode
.PHONY: dev-start
dev-start: image-nginx image-django
	@echo " ---> [$@]"
	PROJECT=$(PROJECT) \
	HOST=$(HOST) \
		$(DC) up --build --detach --remove-orphans \
			--scale django=1

## dev-logs: tail logs
.PHONY: dev-logs
dev-logs:
	@echo " ---> [$@]"
	$(DC) logs -f

## dev-stop: stop docker-compose, dev mode
.PHONY: dev-stop
dev-stop:
	@echo " ---> [$@]"
	$(DC) stop
